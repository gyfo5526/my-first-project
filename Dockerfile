# Use Maven to build the application
FROM maven:3-jdk-8-alpine as builder

WORKDIR /usr/src/app

COPY . /usr/src/app
RUN mvn package

# Use OpenJDK for the runtime environment
FROM openjdk:8-jre-alpine

WORKDIR /

# Copy the compiled JAR file from the builder stage
COPY --from=builder /usr/src/app/target/*.jar /app.jar

# Expose port 8080 for the application
EXPOSE 8080

# Set the entry point and default command to run the application
ENTRYPOINT ["java", "-jar", "/app.jar"]
